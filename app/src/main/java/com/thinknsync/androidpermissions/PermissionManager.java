package com.thinknsync.androidpermissions;

/**
 * Created by shuaib on 5/14/17.
 */

public interface PermissionManager extends ObjectValidator {
    int SMS_PERMISSION_ID = 100;
    int LOCATION_PERMISSION_ID = 101;
    int PHONE_CALL_PERMISSION_ID = 102;
    int READ_PHONE_STATE_PERMISSION_ID = 103;
    int READ_WRITE_STORAGE_PERMISSION_ID = 104;

    boolean isSmsPermissionGranted();
    boolean isLocationPermissionGranted();
    boolean showLocationPermissionMessageIfNeeded();
    boolean isPhoneCallPermissionGranted();
    boolean isImeiPermissionGranted();
    boolean isStorageReadWritePermissionGranted();
}
