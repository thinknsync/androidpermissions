package com.thinknsync.androidpermissions;

import android.Manifest;
import android.content.pm.PackageManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.InvalidWrapperArgumentException;

/**
 * Created by shuaibrahman on 10/17/16.
 */

public class PermissionHandler implements PermissionManager {

    private AppCompatActivity activity;

    public PermissionHandler(AndroidContextWrapper activity) {
        validateObject(activity.getFrameworkObject());
        this.activity = (AppCompatActivity) activity.getFrameworkObject();
    }

    @Override
    public void validateObject(Object activity) {
        try {
            if(!(activity instanceof AppCompatActivity)){
                throw  new InvalidWrapperArgumentException();
            }
        } catch (InvalidWrapperArgumentException e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean isSmsPermissionGranted() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.RECEIVE_SMS}, SMS_PERMISSION_ID);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean showLocationPermissionMessageIfNeeded(){
        if (!isLocationPermissionGranted()) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_ID);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean isLocationPermissionGranted() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean isPhoneCallPermissionGranted() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, PHONE_CALL_PERMISSION_ID);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean isImeiPermissionGranted() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_PHONE_STATE}, READ_PHONE_STATE_PERMISSION_ID);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public boolean isStorageReadWritePermissionGranted() {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE}, READ_WRITE_STORAGE_PERMISSION_ID);
            return false;
        } else {
            return true;
        }
    }
}
